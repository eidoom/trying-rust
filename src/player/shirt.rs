#[path = "shared/colour.rs"]
pub mod colour;

use colour::ColourName;

pub struct Shirt {
    name: String,
    colour: colour::Colour,
}

impl Shirt {
    pub fn new() -> Self {
        Self {
            name: "shirt".to_string(),
            colour: colour::Colour::White,
        }
    }

    pub fn say_name(&self) -> &str {
        &self.name
    }

    pub fn print(&self) -> String {
        format!("  {}\n    Colour: {}", self.name, self.colour.as_str())
    }
}

impl colour::HasColour for Shirt {
    fn set_colour(&mut self, colour: &str) -> String {
        let col = colour.as_colour();
        if col.is_none() {
            return format!("{} is not in my colour palette!", colour);
        }
        self.colour = col.unwrap();
        format!("Set {} colour to {}", self.say_name(), self.say_colour())
    }

    fn say_colour(&self) -> &str {
        &self.colour.as_str()
    }
}
