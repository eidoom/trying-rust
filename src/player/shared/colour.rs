#[derive(PartialEq, Eq, Hash, Clone, Copy)]
pub enum Colour {
    Red,
    Green,
    Blue,
    White,
    Black,
}

impl Colour {
    const MAP: [(Colour, &'static str); 5] = [
        (Self::Red, "red"),
        (Self::Green, "green"),
        (Self::Blue, "blue"),
        (Self::White, "white"),
        (Self::Black, "black"),
    ];

    pub fn as_str(&self) -> &'static str {
        Self::MAP.iter().find(|t| t.0 == *self).unwrap().1
    }
}

pub trait ColourName {
    fn as_colour(&self) -> Option<Colour>;
}

impl ColourName for &str {
    fn as_colour(&self) -> Option<Colour> {
        let res = Colour::MAP.iter().find(|t| t.1 == *self);
        if res.is_none() {
            return None;
        }
        Some(res.unwrap().0)
    }
}

pub trait HasColour {
    fn set_colour(&mut self, colour: &str) -> String;
    fn say_colour(&self) -> &str;
}
