use std::io;

pub fn query(question: &str) -> String {
    println!("{}", question);
    console()
}

pub fn console() -> String {
    let mut input = String::new();

    while {
        while io::stdin().read_line(&mut input).is_err() {
            println!("I'm sorry, I didn't catch that. Could you say it again, please?");
        }
        input == "\n"
    } {
        println!("Come now, that's no answer at all. Could you try again, please?");
        input.clear()
    }

    input.trim().to_string()
}
