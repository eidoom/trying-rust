pub mod shirt;
pub mod trous;

pub struct Player {
    name: String,
    count: u16,
    pub shirt: shirt::Shirt,
    pub trous: trous::Trous,
}

impl Player {
    pub fn new() -> Self {
        Self {
            name: "".to_string(),
            count: 0,
            shirt: shirt::Shirt::new(),
            trous: trous::Trous::new(),
        }
    }

    pub fn set_name(&mut self, new_name: &str) -> String {
        self.name = new_name.to_string();
        format!("Hello {}.", self.say_name())
    }

    pub fn say_name(&self) -> &str {
        &self.name
    }

    pub fn increment(&mut self) -> String {
        let msg = format!("Your move, {}.\n[{}] ", self.say_name(), self.count);
        self.count += 1;
        msg
    }

    pub fn print(&self) -> String {
        format!(
            "\
            Player\
            \n  Name: {}\
            \n{}\
            \n{}\
            ",
            self.say_name(),
            self.shirt.print(),
            self.trous.print(),
        )
    }
}
