mod cli;
mod player;

use std::io::{self, Write};

fn main() {
    let mut p = player::Player::new();

    println!("{}", p.set_name(cli::query("What's your name?").as_str()));

    println!("Remember you can ask for `help` at any time.\nLet's begin.");

    loop {
        print!("{}", p.increment());
        while io::stdout().flush().is_err() {}

        let answer = match cli::console().trim() {
            x if x.get(..4) == Some("set ") => match &x[4..] {
                y if y.get(..6) == Some("shirt ") => {
                    use crate::player::shirt::colour::HasColour;
                    p.shirt.set_colour(&y[6..])
                }
                y if y.get(..6) == Some("trous ") => {
                    use crate::player::trous::colour::HasColour;
                    p.trous.set_colour(&y[6..])
                }
                y if y.get(..5) == Some("name ") => p.set_name(&y[5..]),
                _ => "Um, what do you want to set?".to_string(),
            },

            "status" => p.print(),
            "help" => "\
                ==============================\n\
                help: show this message\n\
                status: show player status\n\
                set name <name>\n\
                set shirt <colour>\n\
                set trous <colour>\n\
                ==============================\
                "
            .to_string(),
            "end" | "quit" | "exit" | "halt" | "stop" => {
                break;
            }
            x if x == p.say_name() => format!("Yes, I know what your name is, {}.", p.say_name()),
            _ => "Um, I don't understand?".to_string(),
        };

        println!("{}", answer);
    }

    println!("Bye then!");
}
