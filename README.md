# [trying-rust](https://gitlab.com/eidoom/trying-rust)

System: Fedora 32

## Basic usage

Install
```shell
sudo dnf install rust
```

Compile
```shell
rustc <program>.rs
```

Run
```shell
./<program>
```

## Cargo

Install
```shell
sudo dnf install cargo
```

This project was initialised with
```shell
cargo init --bin trying-rust
```

Compile
```shell
cargo build
```

Run (also compiles if there are changes)
```shell
cargo run
```

## Reading

* https://stevedonovan.github.io/rust-gentle-intro/readme.html
